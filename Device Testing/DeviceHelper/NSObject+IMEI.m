//
//  NSObject+IMEI.m
//  DeviceTesting
//
//  Created by Conor Pierce on 9/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

#import "NSObject+IMEI.h"
#import "CoreTelephony.h"

@implementation NSObject (IMEI)

struct CTServerConnection *sc=NULL;
struct CTResult result;
void callback() { }

- (NSString *) getIMEI{
    NSString *imei;
    _CTServerConnectionCopyMobileIdentity(&result, sc, &imei);
    return imei;
}
@end
