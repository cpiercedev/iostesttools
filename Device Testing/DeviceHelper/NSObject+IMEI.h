//
//  NSObject+IMEI.h
//  DeviceTesting
//
//  Created by Conor Pierce on 9/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (IMEI)
-(NSString *) getIMEI;
@end

NS_ASSUME_NONNULL_END
