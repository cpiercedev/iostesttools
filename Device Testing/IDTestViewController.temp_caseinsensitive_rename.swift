//
//  IDtestViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/9/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation

class IDTestViewController: UIViewController{
    var number = 0
    var failCount = 0
    var startPressed = false
    var sounds = [String]()
    
    override func viewDidLoad() {
