//
//  sendLog.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import Firebase
import UIKit

var totalUpdated = false
var StoreNumber = ""

func sendLog(controller: UIViewController){
    let userDefaults = UserDefaults.standard
    var testResults: [String] = []
    
    let IMEI = userDefaults.string(forKey: "IMEI")
    let Serial = userDefaults.string(forKey: "Serial")
    let WO = userDefaults.string(forKey: "WO")
    let SystemVersion = userDefaults.string(forKey: "iOSVersion")
    StoreNumber = (userDefaults.string(forKey: "StoreNumber") ?? "N/A")
    let Model = userDefaults.string(forKey: "model")
    let StartTime = userDefaults.object(forKey: "StartTime")
    let EndTime = Date()
    
    
    let LCD = userDefaults.string(forKey: "LCD")
    testResults.append(LCD ?? "False")
    
    let MultiTouch = userDefaults.string(forKey: "MultiTouch")
    testResults.append(MultiTouch ?? "False")
    
    let Charging = userDefaults.string(forKey: "charging")
    testResults.append(Charging ?? "False")
    
    let RearCamera = userDefaults.string(forKey: "RearCamera")
    testResults.append(RearCamera ?? "False")
    
    let FrontCamera = userDefaults.string(forKey: "FrontCamera")
    testResults.append(FrontCamera ?? "False")
    
    let Gyro = userDefaults.string(forKey: "Gyroscope")
    testResults.append(Gyro ?? "False")
    
    
    let Speaker = userDefaults.string(forKey: "Speakers")
    testResults.append(Speaker ?? "False")
    
    
    let IDAuth = userDefaults.string(forKey: "IDAuth")
    testResults.append(IDAuth ?? "False")
    
    
    let Loopback = userDefaults.string(forKey: "Loopback")
    testResults.append(Loopback ?? "False")
    
    
    let Vibration = userDefaults.string(forKey: "Vibration")
    testResults.append(Vibration ?? "False")
    
    
    let Touch3D = userDefaults.string(forKey: "3DTouch")
    testResults.append(Touch3D ?? "False")
    
    let TouchGrid = userDefaults.string(forKey: "TouchGrid")
    testResults.append(TouchGrid ?? "False")
    
//    let Battery = userDefaults.string(forKey: "BatteryHealth")
//    testResults.append(Battery ?? "False")
    
    let NFC = userDefaults.string(forKey: "NFC")
    testResults.append(NFC ?? "False")
    
    let Bluetooth = userDefaults.string(forKey: "Bluetooth")
    testResults.append(Bluetooth ?? "False")
    
    let Proximity = userDefaults.string(forKey: "Prox")
    testResults.append(Proximity ?? "False")
    
    let accel = userDefaults.string(forKey: "Accelerometer")
    testResults.append(accel ?? "False")
    
    let Headphone = userDefaults.string(forKey: "Headphone")
    testResults.append(Headphone ?? "False")
    
    let Ambient = userDefaults.string(forKey: "Ambient")
    testResults.append(Ambient ?? "False")
    
    let Barometer = userDefaults.string(forKey: "Barometer")
    testResults.append(Barometer ?? "False")
    
    let WRT = userDefaults.string(forKey: "WRT")
    testResults.append(WRT ?? "False")
    
    let SIM = userDefaults.string(forKey: "SIM")
    testResults.append(SIM ?? "False")
    
    let Wifi = userDefaults.string(forKey: "Wifi")
    testResults.append(Wifi ?? "False")
    
    let Cell = userDefaults.string(forKey: "Cellular")
    testResults.append(Cell ?? "False")
    
    let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String
    
    let DocID = userDefaults.string(forKey: "DocID")
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    
    print(DocID ?? "N/A")
    
    
    let documentID = DocID ?? "N/A"
    
    let  docRef = db.collection("users").document(documentID);
    
    var result = "Pass"
    for tests in testResults{
        if(tests == "Fail"){
            result = "Fail"
        }
    }
    updateTotal(result: result)
    //logID(result: result)
    var tokenCount = 0
    
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            
            print("\(document.documentID) => \(document.data())")
            print(document.data()!["Tokens"] as! Int)
            
            tokenCount = document.data()!["Tokens"] as! Int
            var uses = document.data()!["Uses"] as! Int
            var passCount = document.data()!["PassCount"] as! Int
            var failCount = document.data()!["FailCount"] as! Int
            
            if(result == "Pass"){
                passCount += 1
            }
            else{
                failCount += 1
            }
            
            print("User has \(tokenCount) tokens")
            tokenCount -= 1
            uses += 1
            print("\(tokenCount) Tokens's remaining")
            docRef.updateData([
                "Tokens": tokenCount,
                "Uses": uses,
                "PassCount": passCount,
                "FailCount": failCount
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
            
        } else {
            print("Document does not exist")
        }
    }
    
    
    
    var WOLink = "https://portal.ubif.net/pos/workorder/" + WO!
    
    var ref: DocumentReference? = nil
    
    ref = db.collection("logs").addDocument(data: [
        "StartTime": StartTime,
        "DateTime": Date(),
        "IMEI": IMEI ?? "N/A",
        "Serial": Serial ?? "N/A",
        "WO": WO!,
        "LCD": LCD,
        "MultiTouch": MultiTouch,
        "StoreNumber": StoreNumber,
        "Charging": Charging,
        "RearCamera": RearCamera,
        "FrontCamera": FrontCamera,
        "Gyroscope": Gyro,
        "Vibration": Vibration,
        "Speaker": Speaker,
        "Pass": result,
        "IDAuth": IDAuth,
        "Loopback": Loopback,
        "Accelerometer": accel,
        "NFC": NFC,
        "Bluetooth": Bluetooth,
        "Touch3D": Touch3D,
        "Headphone": Headphone,
        "Proximity": Proximity,
        "TouchGrid": TouchGrid,
        "Model": Model,
        "AmbientLight": Ambient,
        "Barometer": Barometer,
        "WRT": WRT,
        "SIM": SIM,
        "Wifi": Wifi,
        "Cellular": Cell,
        "WOLink": WOLink,
        "Version": version,
        //"iOS Version": SystemVersion
        
        
        
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                logID(result: result)
                
                let alert = UIAlertController(title: "Log Saved", message: "Remember to delete the app.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
                }))
                
                controller.present(alert, animated: true)
            }
    }
    

}
func getSavedImage(named: String) -> UIImage? {
    if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
        return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
    }
    return nil
}

func updateTotal(result: String){
    if(StoreNumber == "1"){
        return
    }
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    let  docRef = db.collection("TotalUsage").document("Usage");
    
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            
            //var unlocks = document.data()!["TotalUnlocks"] as! Int
            var fail = document.data()!["TotalFail"] as! Int
            var pass = document.data()!["TotalPass"] as! Int
            var tokens = document.data()!["TotalTokens"] as! Int
            if(totalUpdated){
                return
            }
            totalUpdated = true
            if(result == "Pass"){
                pass += 1
            }
            else{
                fail += 1
            }
            tokens += 1
            
            docRef.updateData([
                "TotalTokens": tokens,
                "TotalFail": fail,
                "TotalPass": pass
                
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Total Document successfully updated")
                    }
            }
            
        } else {
            print("Document does not exist TU")
        }
    }
}
func logID(result: String){
    let userDefaults = UserDefaults.standard
    var cell = userDefaults.bool(forKey: "CellularModel")
    var deviceID: String?
    if(cell){
        deviceID = userDefaults.string(forKey: "IMEI")
    }
    else{
        deviceID = userDefaults.string(forKey: "Serial")
    }
    let WO = userDefaults.string(forKey: "WO")
    let StartTime = userDefaults.object(forKey: "StartTime")
    var Model = userDefaults.string(forKey: "model")
    var Nested = "Workorders.\(WO!).Result"
    var NestedAttempts = "Workorders.\(WO!).Attempts"
    var NestedStartTime = "Workorders.\(WO!).StartTime"
    var NestedEndTime = "Workorders.\(WO!).EndTime"
    var NestedFail = "Workorders.\(WO!).FailCount"
    var NestedPass = "Workorders.\(WO!).PassCount"
    let db = Firestore.firestore()
    let settings = db.settings
    var willUpdate = false
    var forcedResult = result
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings

    if let uuid = UIDevice.current.identifierForVendor?.uuidString {
        print(uuid)
        db.collection("Devices").whereField("UUID", isEqualTo: uuid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        //var currentState = document.data()[Nested] as! String
//                        if(currentState == "Pass"){
//                            forcedResult = "Pass"
//                        }
                        //print("\(document.documentID) => \(document.data())")
                    }
                }
        }
        let  docRef = db.collection("Devices").document(deviceID!);
        docRef.getDocument { (document, error) in
            if let document = document {
                if document.exists{
                    
                    docRef.updateData([
                        "LastRunDate": Date(),
                        "UUID": uuid,
                        "Model": Model,
                        Nested: result,
                        NestedStartTime: StartTime,
                        NestedEndTime: Date()
                        
                    ]) { err in
                        if let err = err {
                            print("Error updating document: \(err)")
                            
                        } else {
                            print("Document successfully updated")
                        }
                    }
                    
                } else {
                    print("Document does not exist")

                    
                }
            }
        }
        
}
}
