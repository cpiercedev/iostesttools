//
//  BluetoothViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 11/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//
import Foundation
import UIKit
import CoreBluetooth

class BluetoothViewController: UIViewController, CBCentralManagerDelegate {
    @IBOutlet var testLabel: UILabel!
    @IBOutlet var testIcon: UIImageView!
    
    var testName = "Bluetooth"
    var seguePath = "SegueToAccelerometer"
    
    var manager:CBCentralManager!
    var result = false
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            result = true
            break
        case .poweredOff:
            result = false
            break
        case .resetting:
            print("Bluetooth is resetting.")
            break
        case .unauthorized:
            result = false
            break
        case .unsupported:
            result = false
            break
        case .unknown:
            result = false
            break
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager()
        manager.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        if(!result){
            let alert = UIAlertController(title: "Please turn bluetooth on.", message: "If bluetooth cannot be turned on, manually mark this test as failed.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
            }))
            
            self.present(alert, animated: true)
        }
        else{
            markPass()
        }
    }
    
    func markPass(){
        testLabel.text = "Passed"
        testIcon.image = UIImage(named: "BluetoothPass.png")
        print("\(testName) Passed")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let userDefaults = UserDefaults.standard
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            userDefaults.set("Pass", forKey: self.testName)
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("N/A", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    func markFail(){
        testLabel.text = "Failed"
        print("\(testName) Failed")
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    
    
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            print("Test Not available")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            testLabel.text = "Not Available"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if(singleTest){
                    self.dismiss(animated: true, completion: nil)
                    //self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
                else{
                    self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
            }
        
    }

}
}
