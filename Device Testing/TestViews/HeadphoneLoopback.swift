
import Foundation
import UIKit
import AVFoundation
import MediaPlayer
import Fabric
import Crashlytics

class HeadphoneLoopback: UIViewController {
    
    var testName = "Headphone"
    
    let engine = AVAudioEngine()
    var playerStarted = false
    var playerInitial = false
    var player2 = AVAudioPlayerNode()
    var headphones = false
    @IBOutlet var PassButton: UIButton!
    
    //Setting the default sequential path
    var seguePath = "SegueToSpeaker"
    let volumeView = MPVolumeView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearTap()
        
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        view.addSubview(volumeView)
        
        if let view = volumeView.subviews.first as? UISlider{
            view.value = 0.5 //---0 t0 1.0---
            
        }
        

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        
        clearTap()
        DispatchQueue.main.async {
            let notification = NotificationCenter.default.addObserver(self, selector: #selector(self.handleRouteChange(_:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        }
        
        
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        
        
        
        headphone()
        
        
    }
    @IBAction func FailButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        clearTap()
        userDefaults.set("Fail", forKey: "Headphone")
        sendFail(FailItem: "Headphone")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    @IBAction func PassButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        clearTap()
        
        userDefaults.set("Pass", forKey: "Headphone")
        
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func startLoopback(){
        
        clearTap()
        startTap()
    }
    func stopLoopback(){
        clearTap()
    }
    func headphone(){
        clearTap()
        
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if currentRoute.outputs != nil {
            for description in currentRoute.outputs {
                if description.portType == AVAudioSessionPortHeadphones {
                    
                    startLoopback()
                    
                    DispatchQueue.main.async{
                        self.PassButton.isEnabled = true
                        print("headphone plugged in")
                    }
                    //startStop()
                } else {
                    //print("headphone pulled out")
                   
                    DispatchQueue.main.async{
                        self.PassButton.isEnabled = false
                        print("headphone pulled out")
                    }
                    
                    stopLoopback()
                    //startStop()
                }
            }
        } else {
            print("requires connection to device")
        }
    }
    func clearTap(){
        print("Tap Cleared")
        CLSLogv("%@", getVaList(["Tap Cleared"]))
        engine.stop()
        let input = engine.inputNode
        input.removeTap(onBus: 0)
    }
    func startTap(){
        print("Tap Started")
        CLSLogv("%@", getVaList(["Tap Started"]))
        let input = engine.inputNode
        input.removeTap(onBus: 0)
        let player2 = AVAudioPlayerNode()
        engine.attach(player2)
        
        let bus = 0
        let inputFormat = input.inputFormat(forBus: 0)
        
        engine.connect(player2, to: engine.mainMixerNode, format: inputFormat)
        
        input.installTap(onBus: bus, bufferSize: 512, format: inputFormat) { (buffer, time) -> Void in
            player2.scheduleBuffer(buffer)
        }
        
        try! engine.start()
        player2.play()
    }
    @objc func handleRouteChange(_ notification: Notification) {
        //startStop()
        
        guard
            let userInfo = notification.userInfo,
            let reasonRaw = userInfo[AVAudioSessionRouteChangeReasonKey] as? NSNumber,
            let reason = AVAudioSessionRouteChangeReason(rawValue: reasonRaw.uintValue)
            else { fatalError("Strange... could not get routeChange") }
        switch reason {
        case .oldDeviceUnavailable:
            print("oldDeviceUnavailable")
            CLSLogv("%@", getVaList(["Headphone unavailable"]))
            DispatchQueue.main.async{
                self.PassButton.isEnabled = false
                print("headphone pulled out")
            }
            clearTap()
        case .newDeviceAvailable:
            print("headset/line plugged in")
             CLSLogv("%@", getVaList(["Headphone Plugged in"]))
             DispatchQueue.main.async{
                self.PassButton.isEnabled = true
                //print("headphone plugged in")
             }
            startTap()
        case .routeConfigurationChange:
            print("headset pulled out")
            CLSLogv("%@", getVaList(["Headphone unplugged"]))
            DispatchQueue.main.async{
                self.PassButton.isEnabled = false
                //print("headphone pulled out")
            }
            clearTap()
        case .categoryChange:
            print("Just category change")
        default:
            print("not handling reason")
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    
}
