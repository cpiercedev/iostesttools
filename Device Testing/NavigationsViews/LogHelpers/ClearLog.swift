
import Foundation
import Firebase

func clearLog(){
    let userDefaults = UserDefaults.standard
    //var tests1 = userDefaults.array(forKey: "Tests")
    
    let tests: [String] = [
        "IMEI",
        "WO",
        "3DTouch",
        "LCD",
        "MultiTouch",
        "TouchGrid",
        "charging",
        "Rear Camera",
        "Gyroscope",
        "Speakers",
        "IDAuth",
        "Loopback",
        "vibration",
        "Prox",
        "NFC",
        "Accelerometer",
        "Battery",
        "3DTouch",
        "bluetooth",
        "Barometer",
        "Ambient",
        "WRT",
        "SIM",
        "Wifi",
        "Cell",
        ]
    
    for test in tests{
        userDefaults.set(nil, forKey: test)
    }
    
    //Setting SingleTest as False
    userDefaults.set(nil, forKey: "SingleTest")
    
    
}
