//
//  LCDViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit


class LCDViewController: UIViewController {
    var count = 0
    var seguePath = "SegueToLoopback"
    var testName = "LCD"
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var PassButton: UIButton!
    @IBOutlet var FailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
         checkTestList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func Touched(_ sender: Any) {
        count = count + 1
        switch (count) {
        case 1:
            backgroundView.backgroundColor = .green
            break;
        case 2:
            backgroundView.backgroundColor = .blue
            break;
        case 3:
            backgroundView.backgroundColor = .black
            break;
        case 4:
            backgroundView.backgroundColor = .white
            break;
        case 5:
            PassButton.alpha = 1;
            PassButton.isEnabled = true;
            FailButton.alpha = 1;
            FailButton.isEnabled = true;
            break;
            
        default:
            break;
        }
        
    }
    @IBAction func PassButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard

        userDefaults.set("Pass", forKey: testName)
        
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    @IBAction func FailButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
        
    }
    
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
}
