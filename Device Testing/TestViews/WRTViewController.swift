//
//  WRTViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion
import Charts
import MediaPlayer

class WRTViewController: UIViewController {
    
    
    var testName = "WRT"
    
    
    @IBOutlet var barChartView: BarChartView!
    @IBOutlet var relativeAltitude: UILabel!
    @IBOutlet var testLabel: UILabel!
    @IBOutlet var getReferenceButton: UIButton!
    
    @IBOutlet var pressure: UILabel!
    var seguePath = "SegueToMulti"
    var minPressure = 0;
    var maxAltitude:Double = 0;
    var altitude: Double = 0.0;
    let altimeter = CMAltimeter()
    var testStarted = false
    var testFinished = false
    var testTimes = [Double]()
    var testValues = [Double]()
    var count = 0
    var timeoutCount = 0
    var startTime = Date()
    var elapsedTime:Double = 0
    var referenceAltitude:Double = 0.0
    var referenceCollected = false
    var finalAltitude:Double = 0
    var buttonPressed = false
    var testRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        let storeNumber = userDefaults.integer(forKey: "StoreNumber")
        if(storeNumber == 1){
            checkTestList()
        }
        else{
            userDefaults.set("N/A", forKey: "WRT")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        
        barChartView.chartDescription?.enabled = false
        barChartView.dragEnabled = true
        barChartView.setScaleEnabled(true)
        barChartView.pinchZoomEnabled = true
        
        
        let leftAxis = barChartView.leftAxis
        leftAxis.removeAllLimitLines()
        
        leftAxis.axisMaximum = 700
        leftAxis.axisMinimum = -50
        leftAxis.gridLineDashLengths = [1, 1]
        leftAxis.drawLimitLinesBehindDataEnabled = true
        
        let xAxis = barChartView.xAxis
        xAxis.axisMaximum = 20
        xAxis.axisMinimum = 0
        xAxis.labelFont = .systemFont(ofSize: 11)
        xAxis.labelTextColor = .black
        xAxis.drawAxisLineEnabled = true
        
        barChartView.legend.enabled = false
        barChartView.rightAxis.enabled = false
        updateChart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
    }
    @IBAction func GetReferencePressed(_ sender: Any) {
        buttonPressed = true
        testRunning = true
        if CMAltimeter.isRelativeAltitudeAvailable() {
            altimeter.startRelativeAltitudeUpdates(to: OperationQueue.main) { (data, error) in
                
                if( self.testRunning){
                    self.relativeAltitude.text = String.init(format: "%.1fM",(data?.relativeAltitude.doubleValue)!)
                    //self.pressure.text = String.init(format: "%.2f hPA", (data?.pressure.floatValue)!*10)
                    
                    self.altitude = (data?.relativeAltitude.doubleValue)!
                    if(!self.testStarted && !self.testFinished && self.buttonPressed){
                        self.testLabel.text = "Ready"
                        self.buttonPressed = false
                    }
                    if(self.altitude > 20 && self.altitude < 100){
                        self.timeoutCount += 1
                    }
                    if(self.timeoutCount > 4 && !self.testStarted){
                        self.testLabel.text = "Large leak detected, please check cover tapes"
                    }
                    if(!self.referenceCollected){
                        self.referenceAltitude = self.altitude
                    }
                    if(self.altitude > self.maxAltitude && !self.testStarted && !self.testFinished){
                        self.maxAltitude = self.altitude
                    }
                    else if(self.altitude > 350 && !self.testStarted && !self.testFinished){
                        self.testStarted = true
                        self.testLabel.text = "Test Started"
                        self.getReferenceButton.isEnabled = false
                        self.startTime = Date()
                    }
                    else if(self.count == 20 && !self.testFinished){
                        self.testFinished = true
                        self.finalAltitude = self.altitude
                        self.testLabel.text = "Test Finished"
                        self.testRunning = false
                        self.showData()
                        if(self.altitude > self.maxAltitude*0.95){
                            self.testLabel.text = "Passed"
                            let userDefaults = UserDefaults.standard
                            userDefaults.set("Pass", forKey: "WRT")
                            print("WRT Passed")
                            let singleTest = userDefaults.bool(forKey: "SingleTest")
                            
                            if(singleTest){
                                self.dismiss(animated: true, completion: nil)
                            }
                            else{
                                self.performSegue(withIdentifier: self.seguePath, sender: self)
                            }
                        }
                        else{
                            self.testLabel.text = "Failed"
                            self.getReferenceButton.isEnabled = true
                            self.testTimes = []
                            self.testValues = []
                            self.count = 0
                            self.timeoutCount = 0
                            self.testStarted = false
                            self.testFinished = false
                        }
                    }
                    else if(self.testStarted && !self.testFinished){
                        self.elapsedTime = Date().timeIntervalSince(self.startTime)
                        self.testTimes.append(self.elapsedTime)
                        self.testValues.append(self.altitude)
                        self.updateChart()
                        print(self.testValues)
                        self.count += 1
                        self.pressure.text = "\(self.count)"
                    }
                    
                }
            }
        }
    }
    func showData(){
        updateChart()
    }
    
    
    private func updateChart() {
        var chartEntry = [ChartDataEntry]()
        
        for i in 0..<testValues.count {
            let value = ChartDataEntry(x: testTimes[i], y: testValues[i])
            chartEntry.append(value)
        }
        
        let line = LineChartDataSet(values: chartEntry, label: "Values")
        line.colors = [UIColor.red]
        line.lineWidth = 2
        line.circleRadius = 0
        line.highlightColor = .red
        line.drawCircleHoleEnabled = false
        line.drawValuesEnabled = false
        let data = LineChartData()
        data.addDataSet(line)
        
        barChartView.data = data
        barChartView.chartDescription?.text = "Seconds"
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for WRT", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    func markNA(){
        sendNA(NAItem: "WRT")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "WRT")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "WRT")
        sendFail(FailItem: "WRT")
        
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    
}
