//
//  sendLog.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import Firebase
import UIKit

func sendFail(FailItem: String){
    let userDefaults = UserDefaults.standard
    
    let IMEI = userDefaults.string(forKey: "IMEI")
     let Serial = userDefaults.string(forKey: "Serial")
    let WO = userDefaults.string(forKey: "WO")
    let StoreNumber = userDefaults.string(forKey: "StoreNumber")
    let Model = userDefaults.string(forKey: "model")
    let SystemVersion = userDefaults.string(forKey: "iOSVersion")
    
    
    
    
    var WOLink = "https://portal.ubif.net/pos/workorder/" + (WO ?? "N/A")
    
    var ref: DocumentReference? = nil
    
    let db = Firestore.firestore()
    ref = db.collection("FailedTests").addDocument(data: [
        "DateTime": Date(),
        "IMEI": (IMEI ?? "N/A"),
        "Serial": (Serial ?? "N/A"),
        "WOLink": WOLink,
        "WO": WO,
        "StoreNumber": StoreNumber,
        "Model": Model,
        "FailItem": FailItem,
        "iOS Version": SystemVersion
        
        
        
        
    ]) { err in
        if let err = err {
            print("Error adding document: \(err)")
        } else {
            print("Document added with ID: \(ref!.documentID)")
        }
    }
    
    
}

