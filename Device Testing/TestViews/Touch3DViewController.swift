import UIKit
import Foundation
import MediaPlayer

class Touch3DViewController: UIViewController {
    
    var testName = "3DTouch"
    
    @IBOutlet var Label3D: UILabel!
    @IBOutlet var Label10: UILabel!
    @IBOutlet var Label20: UILabel!
    @IBOutlet var Label40: UILabel!
    @IBOutlet var Label60: UILabel!
    @IBOutlet var Label80: UILabel!
    @IBOutlet var Label100: UILabel!
    var bool10 = false
    var bool20 = false
    var bool40 = false
    var bool60 = false
    var bool80 = false
    var bool100 = false
    var seguePath = "SegueToLCD"
     let mediumImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //checkTestList()
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
       
    }
    override func viewDidAppear(_ animated: Bool) {
         checkTestList()
        
        mediumImpactFeedbackGenerator.prepare()
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        
//        if (model == "iPhone 6s" || model == "iPhone 6s Plus" || model == "iPhone 7" || model == "iPhone 7 Plus" || model == "iPhone 8" || model == "iPhone 8 Plus" || model == "iPhone X") {
//
//        }
//        else{
//            userDefaults.set("N/A", forKey: "3DTouch")
//            performSegue(withIdentifier: "SegueToLCD", sender: self)
//        }
        
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            var force = touch.force/touch.maximumPossibleForce*100
            force.round()
            Label3D.text = String(format: "%0.0f%%", force)
            if(bool10 && bool20 && bool40 && bool60 && bool80 && bool100){
                let userDefaults = UserDefaults.standard
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                userDefaults.set("Pass", forKey: testName)
                //print("Test here")
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            switch force {
            case 10:
                if(bool10 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool10 = true
                Label10.textColor = .green
                
            case 20:
                if(bool20 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool20 = true
                Label20.textColor = .green
               
            case 40:
                if(bool40 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool40 = true
                Label40.textColor = .green
               
            case 60:
                if(bool60 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool60 = true
                Label60.textColor = .green
               
            case 80:
                if(bool80 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool80 = true
                Label80.textColor = .green
                
            case 100:
                if(bool100 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool100 = true
                Label100.textColor = .green
                
                
            default:
                return
            }
        }
        
    }
        
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        Label3D.text = "0%"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func segue(){
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for 3D Touch", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: "3DTouch")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
}
