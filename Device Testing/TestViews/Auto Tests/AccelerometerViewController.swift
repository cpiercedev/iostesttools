//
//  AccelerometerViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 11/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

//
//  AccelerometerViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 11/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import CoreMotion

class AccelerometerViewController: UIViewController {
    
    @IBOutlet var testLabel: UILabel!
    @IBOutlet var testIcon: UIImageView!
    var testName = "Accelerometer"
    var seguePath = "SegueToBarometer"
    
    let motion = CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        accelCheck()
    }
    
    func accelCheck(){
        if self.motion.isAccelerometerAvailable {
          markPass()
        }
        else{
          markFail()
        }
    }
    
    
    func markPass(){
        testLabel.text = "Passed"
        testIcon.image = UIImage(named: "AccelPass.png")
        print("\(testName) Passed")
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //performSegue(withIdentifier: seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    func markNA(){
        
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("N/A", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    func markFail(){
        print("\(testName) Failed")
        testLabel.text = "Failed"
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
    }
    
    
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            print("Test Not available")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            testLabel.text = "Not Available"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if(singleTest){
                    self.dismiss(animated: true, completion: nil)
                    //self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
                else{
                    self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
            }
            
        }
        
    }
}
