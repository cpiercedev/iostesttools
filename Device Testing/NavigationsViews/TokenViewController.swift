//
//  TokenViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/14/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import Firebase

class TokenViewController: UIViewController {
    @IBOutlet var StoreText: UITextField!
    @IBOutlet var TokenText: UITextField!
    @IBOutlet var NewTokenLabel: UILabel!
    @IBOutlet var OldTokenLabel: UILabel!
    @IBOutlet var SuccessLabel: UILabel!
    @IBOutlet var StoreName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func ButtonPressed(_ sender: Any) {
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        let storeNumberAsInt:Int? = Int(StoreText.text!)
        
        db.collection("users").whereField("StoreNumber", isEqualTo: storeNumberAsInt)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {

                        let tokenCount = document.data()["Tokens"]as! Int
                        let totalTokenCount = document.data()["TotalTokens"]as! Int
                        
                        self.OldTokenLabel.text = "Old Token Amount: \(tokenCount)"
                        var tokenAsInt:Int? = Int(self.TokenText.text!)
                        self.StoreName.text = document.data()["StoreName"]as! String
                        
                        let newTokenCount = tokenCount + tokenAsInt!
                        let newTotalTokenCount = totalTokenCount + tokenAsInt!
                        
                        self.NewTokenLabel.text = "New Token Amount: \(newTokenCount)"
                        
                        var unlocks = document.data()["Unlocks"]as! Int
                        let  docRef = db.collection("users").document(document.documentID);
                        unlocks += 1
                        docRef.updateData([
                            "Tokens": newTokenCount,
                            "TotalTokens": newTotalTokenCount
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                let alert = UIAlertController(title: "Error Sending Unlock Data", message: "Please let Conor know if you see this message. Tell him this: \(document.documentID)", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                
                                self.present(alert, animated: true)
                                self.SuccessLabel.text = "Document Updated"
                                
                            } else {
                                print("Document successfully updated")
                                self.SuccessLabel.text = "Document Updated"
                            }
                    }
                }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}
