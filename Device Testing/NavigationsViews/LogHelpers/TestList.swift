//
//  TestList.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/18/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation


func setTestList(){
    //Setting all unniversal tests
    var testList: [String] = [
            "LCD",
            "MultiTouch",
            "TouchGrid",
            "charging",
            "Rear Camera",
            "Gyroscope",
            "Speakers",
            "Loopback",
            "Accelerometer",
            "Bluetooth",
            "Ambient",
            "Wifi",
            "FrontCamera",
            "RearCamera"
        ]
    var defaults = UserDefaults.standard
    
    switch defaults.string(forKey: "model") {
    case "iPod Touch 5":
        testList.append("Headphone")
        break
        
    case "iPod Touch 6":
        testList.append("Headphone")
        break
    case "iPhone 4":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        
    case "iPhone 4s":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        
    case "iPhone 5":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
       
        
    case "iPhone 5c":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        
        
    case "iPhone 5s":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        
        
    case "iPhone SE":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        
    case "iPhone 6":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("Barometer")
    case "iPhone 6 Plus":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("Barometer")
    case "iPhone 6s":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        
    case "iPhone 6s Plus":
        testList.append("Headphone")
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        
    case "iPhone 7":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        testList.append("WRT")
        
    case "iPhone 7 Plus":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
         testList.append("WRT")
        
    case "iPhone 8":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
         testList.append("WRT")
        
    case "iPhone 8 Plus":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
         testList.append("WRT")
        
    case "iPhone X":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        testList.append("WRT")
        
    case "iPhone XR":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        testList.append("WRT")
        
    case "iPhone XS":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        testList.append("WRT")
        
    case "iPhone XS Max":
        testList.append("Vibration")
        testList.append("Prox")
        testList.append("SIM")
        testList.append("Cellular")
        testList.append("IDAuth")
        testList.append("3DTouch")
        testList.append("Barometer")
        testList.append("NFC")
        testList.append("WRT")
        
        
        
        
        ///iPads
    case "iPad 2":
        testList.append("Headphone")
        
        break
    case "iPad 2 Cellular":
        testList.append("Headphone")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad 3":
        testList.append("Headphone")
        break
    case "iPad 3 Cellular":
        testList.append("Headphone")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad 4":
        testList.append("Headphone")
        break
    case "iPad 4 Cellular":
        testList.append("Headphone")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Air":
        testList.append("Headphone")
        break
    case "iPad Air Cellular":
        testList.append("Headphone")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Air 2":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Air 2 Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad 5":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        
        break
    case "iPad 5 Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad 6":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad 6 Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Mini":
        testList.append("Headphone")
        break
    case "iPad Mini Cellular":
        testList.append("Headphone")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Mini 2":
        testList.append("Headphone")
        break
    case "iPad Mini 2 Cellular":
        testList.append("Headphone")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Mini 3":
        testList.append("Headphone")
        testList.append("IDAuth")
        break
    case "iPad Mini 3 Cellular":
        testList.append("Headphone")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Mini 4":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Mini 4 Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Pro 9.7 Inch":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Pro 9.7 Inch Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Pro 12.9 Inch":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Pro 12.9 Inch Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Pro 12.9 Inch 2. Generation":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Pro 12.9 Inch 2. Generation Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Pro 10.5 Inch":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Pro 10.5 Inch Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Pro 11 Inch":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Pro 11 Inch Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break
    case "iPad Pro 12.9 Inch 3. Generation":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        break
    case "iPad Pro 12.9 Inch 3. Generation Cellular":
        testList.append("Headphone")
        testList.append("Barometer")
        testList.append("IDAuth")
        testList.append("SIM")
        testList.append("Cellular")
        break

    default:
        break
    }
    
    defaults.set(testList, forKey: "testList")
}
