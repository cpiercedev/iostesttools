//
//  AmbientLightViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import MediaPlayer
import Foundation

class AmbientLightViewController: UIViewController {
    
    var testName = "Ambient"
    
    var count = 0
    let seguePath = "SegueToGyro"
    var lastBrightness: CGFloat = 0.0;
    var testFinished = false;
    @IBOutlet var AmbientLabel: UILabel!
    @IBOutlet var ProgressBar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
       
        AmbientLabel.text = String(format: "%0.3f%",UIScreen.main.brightness)
        
        NotificationCenter.default.addObserver(self, selector: #selector(brightnessChanged), name: NSNotification.Name.UIScreenBrightnessDidChange, object: nil)
         UIScreen.main.brightness = CGFloat(0.4)
        ProgressBar.setProgress(0.0, animated: true)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        UIScreen.main.brightness = CGFloat(0.4)
    }
    @objc func brightnessChanged(){
        if(UIScreen.main.brightness >= CGFloat(0.9)){
            UIScreen.main.brightness = CGFloat(0.4)
            print("Manually Changed Brightness")
        }
        let currentBrightness = UIScreen.main.brightness
        print(currentBrightness)
        AmbientLabel.text = String(format: "%0.3f%",currentBrightness)
        if(currentBrightness >= lastBrightness){
        count += 1
            print(count)
            updateProgress()
        }
        else{
            print(count)
            count += 1
            updateProgress()
        }
        lastBrightness = currentBrightness
        if(count >= 100){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: testName)
            UIDevice.current.isProximityMonitoringEnabled = false
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                 NotificationCenter.default.removeObserver(NSNotification.Name.UIScreenBrightnessDidChange)
                dismiss(animated: true, completion: nil)
            }
            else{
                if(!testFinished){
                    testFinished = true
                 NotificationCenter.default.removeObserver(NSNotification.Name.UIScreenBrightnessDidChange)
                performSegue(withIdentifier: seguePath, sender: self)
                }
            }
        }
    }
    func updateProgress (){
        let progression = Float(count)/100.0
        ProgressBar.setProgress(progression, animated: true)
        //print(progression)
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for Ambient Light Sensor", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "Ambient")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        sendNA(NAItem: testName)
        if(singleTest){
             NotificationCenter.default.removeObserver(NSNotification.Name.UIScreenBrightnessDidChange)
            dismiss(animated: true, completion: nil)
            
        }
        else{
             NotificationCenter.default.removeObserver(NSNotification.Name.UIScreenBrightnessDidChange)
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "Ambient")
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
             NotificationCenter.default.removeObserver(NSNotification.Name.UIScreenBrightnessDidChange)
            dismiss(animated: true, completion: nil)
        }
        else{
             NotificationCenter.default.removeObserver(NSNotification.Name.UIScreenBrightnessDidChange)
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
