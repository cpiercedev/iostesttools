//
//  TouchableView.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit

protocol segueProtocol: class{
    func segue()
}

class TouchableView: UIView {

    @IBOutlet var ProgressBar: UIProgressView!
    
    
    weak var delegate: segueProtocol?
    
    var touchViews = [UITouch:TouchSpotView]()
    var touchCount = 0
    var moveCount = 0
    var segueCalled = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isMultipleTouchEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isMultipleTouchEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            createViewForTouch(touch: touch)
            touchCount = touchCount + 1
            if (touchCount >= 2){
                
            }
            //print(touchCount)
        }
    }
    func startTimer(){
        
    }
    func resetTimer(){
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let view = viewForTouch(touch: touch)
            // Move the view to the new location.
            let newLocation = touch.location(in: self)
            view?.center = newLocation
            if(touchCount >= 2){
                moveCount = moveCount + 1
                updateProgress()
                
                //print(moveCount)
            }
            if( moveCount > 150 && !segueCalled){
                updateProgress()
                segueCalled = true
                self.delegate?.segue()
            }
            
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            removeViewForTouch(touch: touch)
            touchCount = touchCount - 1
            //print(touchCount)
            if(touchCount < 2){
                moveCount = 0
                updateProgress()
                //print(moveCount)
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            removeViewForTouch(touch: touch)
            touchCount = 0
            moveCount = 0
            updateProgress()
            //print(moveCount)
            
        }
        
    }
    
    // Other methods. . .
    
    func createViewForTouch( touch : UITouch ) {
        let newView = TouchSpotView()
        newView.bounds = CGRect(x: 0, y: 0, width: 1, height: 1)
        newView.center = touch.location(in: self)
    
        // Add the view and animate it to a new size.
        addSubview(newView)
        UIView.animate(withDuration: 0.2) {
        newView.bounds.size = CGSize(width: 100, height: 100)
        }
    
        // Save the views internally
        touchViews[touch] = newView
    }
    func updateProgress (){
        let progression = Float(moveCount)/150.0
        ProgressBar.setProgress(progression, animated: true)
        //print(progression)
    }
    func viewForTouch (touch : UITouch) -> TouchSpotView? {
        return touchViews[touch]
    }
    
    func removeViewForTouch (touch : UITouch ) {
        if let view = touchViews[touch] {
            view.removeFromSuperview()
            touchViews.removeValue(forKey: touch)
        }
    }
}
